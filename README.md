Bascis of Open Source Hardware
===

## General

This is a teaching module. Find its sections below.

Slides rely on [reveal.js](https://revealjs.com/) and are written in markdown.

To launch them in a presentable mode just copy the markdown source into a new document of any CodiMD instance (e.g. [here](http://demo.codimd.org/)).

### file name convention

It's fairly easy:

[type]-[module]-[sub-module]

S – slides
G – group action

EXAMPLE 1:

`S-intro-osh.md` = slides for [Introduction module](#introduction), specifically for the OSH part (as there're also slides for the entrepreneurship part in the same module)

EXAMPLE 2:

`S-docu.md` = slides for [Documentation module](#documentation), apparently there're no submodules

EXAMPLE 3:

`G-deep-value.md` = description for group action within the [Deep Dive module](#deep-dive), specifically the value creation exercise

## 12.11. Introduction

- What is this course about?
- presentation of all lecturers
- entrepreneurship basics
- Why, What, How OSH + examples
- potentials for entrepreneurship

**Plan:**

- Sigrid: Intro [15min]
- _ventilate_
- Sigrid: general introduction to Business Models [60min]
- _ventilate_
- Martin: Introduction to Open Source Hardware [60min]
    - [slide show](https://pad2.opensourceecology.de/p/Sk4sMZeYD#/) ([source](slides/s-intro-osh.md))
- _ventilate_
- Sigrid: Group forming & discussion [90min]
    - [Group] What should be open and why?
        - [group action: opening](group-actions/G-intro-opening.md)

## 19.11. Documentation of OSH

- what is the key difference between open and closed documentation?
- why is open source documentation not a single checklist?
- what are the main elements (to start with) of Open Source Projects?
- Example Solar Charger
- what happens when you build your own hardware and look at it from the documentations side?
- from documentation to collaboration
- platforms, tools, improvements

**Plan:**
- Sigrid: Intro [15min]
- _ventilate_
- Timm: Why documentation is key and a never ending story [45min]
    - [slide show](https://pad2.opensourceecology.de/p/BJwuhtaYP#) ([source](slides/s-intro-docu.md))
- _ventilate_
- Timm: Exercise part 1 - How to build a simple Solar Charger [90min]
    - → introduction (tools, materials, online-collaboration) + FAQ [15]
    - → form groups + preparation [15]
    - → prototyping + documentation [60]
- _ventilate_
- Timm: Exercise part 2 - Decentralised collaboration [60min]
    - → new setup - new challenge
    - → how to improve collaboration?
    - → what are the learnings and what is next?
- _ventilate_
- Sigrid: Feedback and Checkout [15]


## 26.11. Deep Dive into OSH; Entrepreneurship for OSH

- IP Law basics & role of OSH
    - crush the patent illusion
    - IP law basics (copyright & patent law)
    - tl;dr OSH licenses & compatibility
- How to make it open?
    - Documentation blocks (editable source files etc.)
- Open revenue streams
- current efforts: standardisation & OSH
- guest: DIN

**Plan:**

- Martin: Deep Dive into OSH [60min]
    - [slide show](https://pad2.opensourceecology.de/p/H13RyulYw#/) ([source](slides/s-deep-osh.md))
    - [group action: poor patent](group-actions/G-deep-poorpatent.md) [50min]
- Sigrid: Open Entrepreneurship [60min]
- _ventilate_
- Martin & Sigrid: Open value proposition [60min]
    - [group action: value proposition](group-actions/G-deep-value.md)
- Martin: DIN SPEC 3105 [15min]
- _ventilate_
- Amelie: DIN [30min]
