[Group] Build & document action
===

## Meta

- 2½h
- 4 person per team
  - 3 physical present
  - 1 online
- split among x different rooms

## Plan

### Preparation [30min]

- introduction (tools, materials, online-collaboration) + FAQ [15]
- form groups + preparation [15]

### 1st round | build the thing [1h]

- follow instructions
- document on xxx

### Interruption

- trainer announces the unexpected end of the exercise

### 2nd round | swap [1h]

- continue a different group's work
- condense learnings
