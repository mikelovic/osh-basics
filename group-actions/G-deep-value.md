[Group] Open value proposition
===

## Meta

- 60min
- class is already divided in 10 groups of 3…5 

## Plan

### Introduction [10min]

- for each group 1 of the following 5 products is randomly assigned, so for 10 groups we get 2 groups per product
    - bicycle
    - e-scooter
    - COVID-19 ventilator
    - laptop
    - smartphone
- Groups can swap products if they really want to.
- Situation: Each group just won a public grant to make this product a) open source and b) suitable for a sustainable business model
- optional:
    - assign roles in group:
        - moderator & documenter, guiding & documenting the discussion 
        - dreamer, providing unfiltered ideas
        - realist, translating the dreamers ideas into things that could actually work (neutral perspective!)
        - critic, finding gaps and defects of ideas after processed by the realist
    - one iteration loop goes from dreamer to realist to critic, 1min each, moderator guides this (while keeping track of the time)
    - the next iteration loop starts again with the dreamer who now can provide ideas to fill & fix the gaps & defects detected by the critic

### Create a value proposition canvas [35min]

- each group fills out the value proposition canvas template

as here <https://docs.google.com/presentation/d/1XYQ3heTDzwhE9MG5j6aDHJZu052VmxgPmObQiBir0Oo/edit#slide=id.g7004d9dd22_0_15>

### Compare [15min]

- Groups that worked on the same product sit together and compare & discuss their results

---

for more:

(### Open canvas)

as here <https://docs.google.com/presentation/d/1XYQ3heTDzwhE9MG5j6aDHJZu052VmxgPmObQiBir0Oo/edit#slide=id.g70e6da95e3_0_60>