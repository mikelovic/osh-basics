[Group] Patent vs. Open Dynamics
===

## Meta

- 50min
- explain the rounds as they come; **don't disclose the whole plan from the beginning** so the 'surprise' can work

## Plan

### 1st round | think for yourself [10min]

- choose one of the following product categories
    - bicycle
    - e-scooter
    - COVID-19 ventilator
    - laptop
    - smartphone
- create 1 idea that would bring something new into this category
- narrow it down to its main inventive points and note them (in a MD sheet) 

### 2nd round | defend your patent [20min]

- move into the breakout room of your category
- the first person (A) entering the room holds a patent for his/her idea
- once everyone is in person A briefly (30sec) reads out the main inventive points of his/her patent
- now the others try to claim a patent for their idea as well without infringing the existent one
    - readjust your inventive points when they collide with the existent patent and note them (in a MD sheet, shared with the rest of the group)
    - once you are sure about it, announce that you have a patent and briefly (30sec) read out its main inventive points to the others
        - the others **listen**
        - even if the new patent is infringing yours, don't say anything yet, you'll sue them later
    - this process continues until everyone has a patent
- group discussion: Has your patent been infringed? By whom and in which points?
- move back into the common room and talk about your experience

### 3rd round | collaborate [20min]

- forget your patent; choose a CERN OHL v2.0 variant for your idea
    - CERN-OHL-P: permissive, no copyleft → adoptions can be proprietary
    - CERN-OHL-W: weak copyleft → adoptions must be open, but it can be included in a proprietary framework
    - CERN-OHL-S: strong copyleft → adoptions must be fully open
- move back into the breakout room of your category
- briefly (30sec) explain your original ideas again and discuss where it makes sense to
    - copy the ideas of others
    - collaborate on similar aspects in product development
    - change your idea when you see that others are already working on it so you can do something else you like more
- move back into the common room and talk about your experience
