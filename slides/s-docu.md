---
type: slide
slideOptions:
  transition: slide
---

<img src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo.svg" style="border: none;background: none;box-shadow:none" height="600">

---

# Documentation

**of**

## Open Source Hardware 

These are Open Educational Resources (OER)
used under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

[source](https://gitlab.com/osh-academy/osh-basics/-/blob/master/slides/s-intro-docu.md)

>[19.11.2020 - Timm Wille](https://wiki.opensourceecology.de/En:Timm_Wille)

<!--- 45min in total -->

---

## Recap OSH Basics

---

## What is Open Source Hardware?

<span>"[…] hardware whose design is made publicly available so that anyone can 
    study, modify, distribute, 
    make, and sell 
    the design or hardware based on that design."<!-- .element: class="fragment" data-fragment-index="1" --></span> 
<span>([OSHWA](https://www.oshwa.org/definition))<!-- .element: class="fragment" data-fragment-index="1" --></span> 


Note:
    - That's the definition from the Open Source Hardware Association.
    - So no dependencies.
    - You are publishing design files and just anyone could build and sell the hardware.

---

## Why does Openess matter?

Open Source Hardware can…

- <span>speed up innovation<!-- .element: class="fragment" data-fragment-index="1" --></span> 
- <span>get extra skills on board<!-- .element: class="fragment" data-fragment-index="2" --></span> 
- <span>…<!-- .element: class="fragment" data-fragment-index="3" --></span> 

Note: 
    - repeat from intro slides (headhunting etc.)
    - more ...

---

### Excample Circular Economy

<iframe width="1024" height="576" data-src="https://www.youtube.com/embed/dJ8DIn2vEV0?start=48" allowfullscreen data-autoplay></iframe>

Note:
    adjust video start point in the link

---

## Short Brainstorm

## What is the purpose of documentation?

Note:
    - grab your neighbour and discuss
    - write downs the essencials 
    - why, what, for who, collaboration, ip

---

## What is the difference between open and closed technical documentation?

theoretically **none** except the accessibility and degrees of freedom

---

## ++Open Source++ Hardware brings new challenges

- Openness → licenses
- Platform → community discussion
- Source → accessibility to files and information
- Guidance → stakeholder collaboration and contribution
- Release → constant evolution and version controle

---

as well as 
- Readme → what is it all about?
- Forum → issue management and communication
- Read Only → some source information will need special output formats 
- Maintenance → Response, Quality, Improvements - nothing happens on its own, keep it allive 

Note:
    - Branches, Forks, Modularity, Scalability, constant Improvements
    - Business Case, Service, Moderation, challenges

---

## 4 Degrees of Documentation
Know who you write the documentation for
- contribution guide 
- how to guide 
- user tutorials
- developer guide 



https://opensource.com/article/17/5/five-steps-documentation

---

<!-- .slide: data-transition="zoom" -->

# Exercise 

---

## Now it's time to get hands on experience

---

## How to build a simple solar charger

... so others can build upon what you have started

<img src="https://github.com/opencultureagency/Solar.mini/raw/master/DSC_9193.JPG" style="border: none;background: none;box-shadow:none" height="400">


---

<!-- .slide: data-background="https://github.com/opencultureagency/Solar.mini/raw/master/OpenHardwareGuide_5_A3-solar.mini.png" data-background-color="#000" -->

[Circuit diagram]

note that you just need to replace "blob" by "raw" to display pictures from git-repositories

---

## Tools & Materials

- <span>tool 1<!-- .element: class="fragment" data-fragment-index="1" --></span> 
- <span>tool 2<!-- .element: class="fragment" data-fragment-index="2" --></span> 
- <span>tool 3<!-- .element: class="fragment" data-fragment-index="3" --></span>
- <span>tool 4.1 appearing<!-- .element: class="fragment" data-fragment-index="4" --></span>
- <span>tool 4.2 at once<!-- .element: class="fragment" data-fragment-index="4" --></span>
- <span>tool 4.3 via 'data-fragment-index'<!-- .element: class="fragment" data-fragment-index="4" --></span> 

→ separate Repository?

Choose a <span><!-- .element: class="fragment highlight-red" data-fragment-index="5" -->license</span> <span>← super important<!-- .element: class="fragment" data-fragment-index="5" --></span> 

---

# FAQ
---

## Group action 

note that

``` markdown
# FAQ

---
 
## Group action
```

would result in 2 separate slides 
due to the extra empty line

