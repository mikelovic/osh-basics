---
type: slide
slideOptions:
  transition: slide
---

<img src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo.svg" style="border: none;background: none;box-shadow:none" height="600">

---

# Open Source Hardware 

These are Open Educational Resources (OER)
used under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

[source](https://gitlab.com/osh-academy/osh-basics/-/blob/master/slides/s-intro-osh.md)

[12.11.2020 - Martin Häuer]

<!--- 60min in total -->

---

## What is open source?

**= knowledge shared**

- <span>to everyone<!-- .element: class="fragment" data-fragment-index="1" --></span> 
- <span>forever<!-- .element: class="fragment" data-fragment-index="2" --></span> 
- <span>everywhere<!-- .element: class="fragment" data-fragment-index="3" --></span> 

Note:
    Well this sounds much like Wikipedia, right?

---

![](https://upload.wikimedia.org/wikipedia/commons/8/80/Wikipedia-logo-v2.svg =400x400)

Note:
    - Wikipedia is an open source encyclopaedia which outcompeted all these super-expensive printed versions - still remember them?
    - As content is open source there, everyone can make use of it as he/she likes. You could even make a collection of your favourite articles, print and sell them as a book.
    - Also it's software base - Wikimedia - is open source.
    [Q] Do you have some everyday example for open source?

---

![image alt](https://upload.wikimedia.org/wikipedia/commons/a/a3/Eq_it-na_pizza-margherita_sep2005_sml.jpg "Pizza" =400x400) ![image alt](https://upload.wikimedia.org/wikipedia/commons/d/d2/Pythagorean.svg "Pizza" =400x400)

Note:
    - Everyone knows how to make Pizza, there's no copyright on the recipe.
    - Same for the Theorem of Pythagoras - large parts of our educational (and socio-economic) system wouldn't work if _everything_ would be proprietary.
    - The same applies for our IT infrastructure →

---

<span>![](https://upload.wikimedia.org/wikipedia/commons/3/3a/Tux_Mono.svg =214x258)<!-- .element: class="fragment" data-fragment-index="1" --></span>

|       |                      | 
| ----- | -------------------- | 
| 100 % | supercomputers       | 
| 95 %  | servers (top 1 Mio.) |
| 75 %  | mobile devices       |
| 70 %  | embedded systems     |

<!---
sources:
https://itsfoss.com/linux-runs-top-supercomputers/ bzw. https://www.top500.org/statistics/details/osfam/1/
https://www.zdnet.com/article/can-the-internet-exist-without-linux/
https://www.embedded.com/wp-content/uploads/2019/11/EETimes_Embedded_2019_Embedded_Markets_Study.pdf
https://gs.statcounter.com/os-market-share/mobile/worldwide
-->

Note:
    - You probably heard of the largest software project of human history, the kernel of the by far most used operating system, powering xx % of all xx.
    - Linux.

---

<span>Proprietary software
    contains **40…60%** free/open source code.<!-- .element: class="fragment" data-fragment-index="1" --></span> 
<div></div>
<span>No one writes software from scratch today.<!-- .element: class="fragment" data-fragment-index="2" --></span> 
<div></div>
<span>The same can/will happen for hardware.<!-- .element: class="fragment" data-fragment-index="3" --></span> 

<!---
sources:

Flexera Report „State of Open Source License Compliance 2020“
https://www.zdnet.com/article/60-percent-of-codebases-contain-open-source-vulnerabilities/
https://www.helpnetsecurity.com/2018/05/22/open-source-code-security-risk/
-->

Note:
    And not only that:
    [Q] What do you think is the average percentage of open source code in proprietary code?

---

## What is open source ++hardware++?

<span>"[…] hardware whose design is made publicly available so that anyone can 
    study, modify, distribute, 
    make, and sell 
    the design or hardware based on that design."<!-- .element: class="fragment" data-fragment-index="1" --></span> 
<span>([OSHWA](https://www.oshwa.org/definition))<!-- .element: class="fragment" data-fragment-index="1" --></span> 

Note:
    - That's the definition from the Open Source Hardware Association.
    - So no dependencies.
    - You are publishing design files and just anyone could build and sell the hardware.

---

## Is business even possible then?

<span>Ever bought 
    a pizza?<!-- .element: class="fragment" data-fragment-index="1" --></span> 
<span>or screws?<!-- .element: class="fragment" data-fragment-index="2" --></span>
<span>or embedded systems running linux?<!-- .element: class="fragment" data-fragment-index="3" --></span>
<span>![image alt](https://upload.wikimedia.org/wikipedia/commons/3/34/Linksys-Wireless-G-Router.jpg =320x300)<!-- .element: class="fragment" data-fragment-index="3" --></span>

---

## Some businesses are only possible ++because++ they are (partly) open.

---

## Let's ventilate

![](https://gitlab.com/osh-academy/osh-basics/-/raw/master/graphics/noun_Wind_3377555.svg =400x400)

---

## Open Source Hardware

## in practice

---

## [Arduino](https://www.arduino.cc/)

<iframe width="1024" height="576" data-src="https://www.youtube.com/embed/UoBUXOOdLXY?start=156" allowfullscreen data-autoplay></iframe>

<!--- list of perfect ratios https://antifreezedesign.wordpress.com/2011/05/13/permutations-of-1920x1080-for-perfect-scaling-at-1-77/ -->

---

<!-- .slide: data-background="https://raw.githubusercontent.com/ProbotXYZ/EggBot/master/EggPainter.png" data-background-color="#000" -->

## [Eggprinter](https://www.thingiverse.com/thing:2245428)

---

<!-- .slide: data-background="https://raw.githubusercontent.com/ProbotXYZ/EggBot/master/AssemblyInstructions/Assembly.png" data-background-color="#000" -->

<span>![](https://raw.githubusercontent.com/ProbotXYZ/EggBot/master/Samples/More%20Eggbot%20Op%20Art/preview.jpg)<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

<!-- .slide: data-background="https://gitlab.com/osh-academy/osh-basics/-/raw/master/graphics/Safecast_bGeigie_Nano_opened.jpg" data-background-color="#000" -->

## [Safecast](https://safecast.org/)

![image alt](https://upload.wikimedia.org/wikipedia/en/7/77/Safecast_Tile_Map_screenshot.jpeg) Fukushima crisis response

---

<!-- .slide: data-background="https://i2.wp.com/fossa.systems/wp-content/uploads/2019/10/66748130_891630314507139_3361097979711717376_n.jpg" data-background-color="#000" -->

## [FOSSASAT](https://fossa.systems/fossasat-1/)

---

<!-- .slide: data-background="https://i.ytimg.com/vi/gXP01bjIwHo/maxresdefault.jpg" data-background-color="#000" -->

## [LifeTrac](https://wiki.opensourceecology.org/wiki/LifeTrac_6)

Open Source Ecology (US)

---

<!-- .slide: data-background="https://icdn3.digitaltrends.com/image/digitaltrends/farmbot-express-feat-3251132-2189x1459.jpg" data-background-color="#000" -->

## [Farmbot](https://farm.bot/)

---

<!-- .slide: data-background="https://images.ctfassets.net/mu8m5cabjuvl/41OKPH0EKvv3HtT9bl6fPU/d232e447784bb70e972ab9430df58e0a/Plastic_Products.jpg" data-background-color="#000" -->

## [Precious Plastic](https://preciousplastic.com/)

---

## OSH <span><!-- .element: class="fragment highlight-red" -->≠</span> DIY

Note:
    - Even though lots of OSH projects are designed to be replicated by anyone, I want to point out that OSH IS NOT DIY
    - DIY:
      - limited to what you can do yourself
      - often unclear license
      - commercial use not considered

---

| enterprise | annual revenue |
| ---------- | -------------- |
| Arduino    | `$` 161.9m     |
| Sparkfun   | `$` 72.6m      |
| Prusa      | `$` 50.2m      |
| …          |                |

<!---

sources: 

https://ecommercedb.com/en/store/arduino.cc
https://ecommercedb.com/en/store/sparkfun.com
https://www.wikidata.org/wiki/Q27923775

-->

---

<!-- .slide: data-background="https://3s81si1s5ygj3mzby34dq6qf-wpengine.netdna-ssl.com/wp-content/uploads/2020/05/ab_facebook-ocp-racks-server.jpg" data-background-color="#000" -->

## [Open Compute Project](https://www.opencompute.org/)

---

<!-- .slide: data-background="https://upload.wikimedia.org/wikipedia/commons/d/db/Reprap_Darwin_2.jpg" data-background-color="#000" -->

## [RepRap](https://reprap.org/)

---

<iframe width="1024" height="576" data-src="https://www.opensourceimaging.org/"></iframe>

---

## Open market effects

### <span>1. Impact<!-- .element: class="fragment" data-fragment-index="1" --></span>

<span>also…<!-- .element: class="fragment" data-fragment-index="2" --></span>

- <span>market penetration (Android)<!-- .element: class="fragment" data-fragment-index="2" --></span>
- <span>free supply chains (CERN)<!-- .element: class="fragment" data-fragment-index="3" --></span>
- <span>community (headhunting)<!-- .element: class="fragment" data-fragment-index="4" --></span>

Note:
    Why would people do that?
    Intrinsic factor: Impact (first & foremost)
    Extrinsic factors: market penetration…
    But of course there are challenges →

---

## Business challenges:

- <span>less dependencies<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>experimental path<!-- .element: class="fragment" data-fragment-index="2" --></span>
- <span>new tools, tasks and legal issues<!-- .element: class="fragment" data-fragment-index="3" --></span>

Note: 
    - dependencies are a solid base for commercial business models
    - sacrifice a fairly predictable path
    - …for new tools, tasks and legal issues


---

## Why bother?

---

## What's the point of your business?

<!---
There are ways to use open source just for commercial business. Google's Android showed how well market penetration works using open source.
--->

---

> “Much as Wikipedia has sought to democratize access to knowledge 
> and the open source software movement has attempted to democratize computing,
> Open Source Ecology seeks to democratize human wellbeing
> and the industrial tools
> that help to create it.”
([Open Source Ecology](https://www.mitpressjournals.org/doi/pdf/10.1162/INOV_a_00139))

---

## Let's ventilate

![](https://gitlab.com/osh-academy/osh-basics/-/raw/master/graphics/noun_Wind_3377555.svg =400x400)

---

## The basis for a circular economy

---

## What if…

Companies would accept the return
of all products they ever sold so
they would recycle them?

Note:
    [Discussion]

---

### …we'd get zillions of parallel individual loops.

([talk](https://youtu.be/dJ8DIn2vEV0))

<span>**How about connecting them?**<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

## Jobs to connect

### in a circular economy

We can connect local…

- development
- production
- <span><!-- .element: class="fragment" data-fragment-index="1"-->maintenance</span> 
- <span><!-- .element: class="fragment" data-fragment-index="1"-->modification</span> 
- <span><!-- .element: class="fragment" data-fragment-index="1"-->additional services</span> 
- <span><!-- .element: class="fragment" data-fragment-index="1"-->refurbishment</span> 
- <span><!-- .element: class="fragment" data-fragment-index="1"-->recycling</span> 
- <span><!-- .element: class="fragment" data-fragment-index="1"-->…</span> 

Note:
    [discussion]

---

## That's open source hardware!

If we publish the <span><!-- .element: class="fragment highlight-red" data-fragment-index="1"-->complete documentation</span> 
under a <span><!-- .element: class="fragment highlight-red" data-fragment-index="1"-->free/open license</span>, anyone is able to
locally study, produce, maintain, modify […]
the hardware.

Note:
    And that's happening! →

---

## Community, community, …

We, the OSH community, are building
a circular, open society,
"democratising human wellbeing".
We construct rather than complain.

### <span>Now, who's "we"?<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

<!-- .slide: data-transition="zoom" -->

## We is you.

---

## What do ++you++ want to see as

## open source?

---

<!-- .slide: data-transition="convex" -->

# Group action

---

## Open products & services

<!-- .slide: style="text-align: left;"> --> 

#### 1. Think for yourself [5min]
    
    What should be open source – and why?

#### 2. Group challenge [20min]

    Pitch your idea in the group
    …while the group challenges your idea.
    --
    What's the use case? How is it better? 
    Who are your users? Who are your contributors?…

#### 3. Pitch [∑=10min]

    1 person per group 
    to pitch 1 idea
    in 1min
    to the whole course
