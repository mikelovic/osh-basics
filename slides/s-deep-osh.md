---
type: slide
slideOptions:
  transition: slide
---

# Deep Dive into

<img src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo.svg" style="border: none;background: none;box-shadow:none" height="400">

---

## Deep Dive into Open Source Hardware
---

These are Open Educational Resources (OER)
used under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

[source](https://gitlab.com/osh-academy/osh-basics/-/blob/master/slides/s-deep-osh.md)

[26.11.2020 - Martin Häuer]

<!--- 60min in total -->

---

## Lessons to learn

1. How IP law works (for us).

2. How to open the source.

3. How to fund development.

Note:
    The first session was fun, today we have some work.
    This will be lots of legal stuff.
    Consider this a summary of lots of legal consultation hours that would have cost loads of money

---

(For us) there are essentially 2 mutually
exclusive fields:

## 1. Copyright

## 2. Patent law

## <span>+ Trademarks<!-- .element: class="fragment" data-fragment-index="1" --></span> 

---

## Copyright
---

…gives the owner the exclusive right to use and to make copies of a <span><!-- .element: class="fragment highlight-red" data-fragment-index="2" -->creative</span> work.

<span>--
    Official 'sharing' of your work requires a license.<!-- .element: class="fragment" data-fragment-index="1" --></span>

Note:
    [Drawing action](lecture-actions/L-deep-osh-draw.md)
    A license is sort of a standard contract with users.
    So, you want to broadcast my movie? Pay this fee and stick with those rules and everything will be fine.


---

"[no one can copyright an]
idea, procedure, process, system, method of operation, concept, principle, or discovery."

    Copyright Act, Section 102(b)

Note:
    For that we have… you guess it →

---

## Patents
---
…gives the owner the exclusive right to make, use or sell an invention […].

<span>--
    …shareable by a license.<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

### How to distinguish products from different manufacturers?

---

## Trademark™
---

…identifies products or services of a particular source.

|      |      |             |      |
| ---- | ---- | ----------- | ---- |
| name | logo | catchphrase | etc. |

<span>Open source action figures can be built by anyone, but only the trademark holder can sell Hulk.<!-- .element: class="fragment" data-fragment-index="1" --></span>

Note:
    So let's get more practical →

---

# IP Law
---

in times of open source hardware

Note:
    Anyone knows what 'IP' stands for?
    → Intellectual Property

---

## Old world says:

> If you have a great idea:
> Go and patent it!

---

<!-- .slide: data-transition="fade" -->

<img class="r-stretch" src="https://gitlab.com/osh-academy/osh-basics/-/raw/master/graphics/forbes-screenshot.png">

([article](https://www.forbes.com/sites/stephenkey/2017/11/13/in-todays-market-do-patents-even-matter/#4e2c9d2c56f3))

---

<!-- .slide: data-transition="fade" -->

## How is that?

---

- inventions must be novel & nonobvious
- patents are costly
- patenting takes a while

---

> "Everything that can be invented has been invented."

    Henry L. Ellsworth, commissioner of the U.S. Patent Office,
    1843 report to Congress

<!--- source:
https://en.wikiquote.org/wiki/Patent#Misattributed
-->

Note:
    Well, we all know that's not true - especially in 1843.
    But since patentable ideas must be _new_ without infringing an existent patent…

---

After almost 550 years of patent law

<img src="https://gitlab.com/osh-academy/osh-basics/-/raw/master/graphics/noun_Missing Puzzle Piece_1445716.svg" style="border: none;background: white;box-shadow:none" height="400">

it's hard to find patentable gaps.

<span>+ keep in mind patents aren't for free<!-- .element: class="fragment" data-fragment-index="1" --></span>

<!--- source:
https://en.wikipedia.org/wiki/History_of_patent_law#cite_note-1
-->

---

<span>filing<!-- .element: class="fragment highlight-red" data-fragment-index="1" --></span> a patent for main markets

(EU,    GB,    CN,    JP,    US)

=

### 30.000…100.000+ €

`…per idea to protect.`

<span>Make sure you need it.<!-- .element: class="fragment" data-fragment-index="2" --></span>


<!--- source:
[german] https://www.patent-pilot.com/de/ein-patent-anmelden/kosten-der-patentanmeldung/
-->

---

using a patent

in trial

=

### 100.000…30.000.000 € 

<span>Can you afford using your patent(s)?<!-- .element: class="fragment" data-fragment-index="1" --></span>

<!--- source:
https://preubohlig.de/wp-content/uploads/2019/07/PatentLitigationHoppe.pdf
https://apnews.com/press-release/news-direct-corporation/a5dd5a7d415e7bae6878c87656e90112
-->

Note:
    avg=3.6m€
    similar in the US

---

Once filed, the application procedure
takes <span>3…5 years<!-- .element: class="fragment" data-fragment-index="1" --></span> (in the EU).

<span>How long is your product lifespan?<!-- .element: class="fragment" data-fragment-index="2" --></span>

Note:
    [Q] How long does the process take?
    lifespan of consumer products: ~18 months

<!--- source:
https://www.epo.org/service-support/faq/own-file.html
-->

---

## But:

"Patents give you <span>a guarantee in trial."<!-- .element: class="fragment fade-out" data-fragment-index="1" --></span>

<span>a seat at the table, both offensively and defensively."<!-- .element: class="fragment" data-fragment-index="1" --></span>
    
([Forbes](https://www.forbes.com/sites/stephenkey/2017/11/13/in-todays-market-do-patents-even-matter/?sh=2edbefab56f3))


Note:
    Unfortunately that's not the case.
    value (and legitimacy) of a patent shows off in trail
    **[Q]** What's offensive/defensive use?

---

break

---

|  | Copyright | Patent | Trademark |
| -------- | -------- | -------- | ----- |
| for | creative act | invention | identity |
| applies | by default | when filed | when registered |
| costs | - | €€€ | € |
| lasts | ~100 years | ≤20 years | as long as paid |

---

## How to open the source?

---

### Hardware

=

- <span>functional parts<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>technical documentation<!-- .element: class="fragment" data-fragment-index="2" --></span>
- <span>software<!-- .element: class="fragment" data-fragment-index="3" --></span>
- <span>art stuff<!-- .element: class="fragment" data-fragment-index="4" --></span>
- <span>…<!-- .element: class="fragment" data-fragment-index="4" --></span>

---

| patent law | ~~~ |  | copyright |
| -------- | -------- | -------- | -------- |
| functional parts | technical documentation | software | art stuff |

Note:
    On the left side where people say, no creativity is required, things become automatically open once we publish them
    On the right side we open things via a license
    The fuzzy line between both concepts is somewhere in the 

---

Patentable ideas have to be

- <span>novel<!-- .element: class="fragment highlight-red" data-fragment-index="1" --></span>
- nonobvious

<span><!-- .element: class="fragment" data-fragment-index="2" --></span>

### <span>You can't patent 'state of the art'.<!-- .element: class="fragment" data-fragment-index="2" --></span>

Note:
    …and using this mechanism is called →

---

## Defensive Publishing

An idea that has been published
in <span>…<!-- .element: class="fragment fade-out" data-fragment-index="1" --></span> <span>_any part of the world_<!-- .element: class="fragment" data-fragment-index="1" --></span>

cannot be patented anymore
in Germany. <span>Or elsewhere.<!-- .element: class="fragment" data-fragment-index="1" --></span>

### <span>'published'<!-- .element: class="fragment" data-fragment-index="2" --></span>
<span>=<!-- .element: class="fragment" data-fragment-index="2" --></span>
<span>resilient timestamp + public access<!-- .element: class="fragment fade-up" data-fragment-index="2" --></span>

Note:
    Something published in Hungary cannot be patented in Japan anymore
    So if you just once told your friend: no timestamp, no public access
    If you published it on GitLab: resilient timestamp, public access

---

What about the (large) creative rest?

---

## Copyright & Copyleft

---

| | |
| -------- | -------- |
| <img src="https://upload.wikimedia.org/wikipedia/commons/b/b0/Copyright.svg" style="border: none;background: white;box-shadow:none"> | work is closed by default, license use, modification etc. (for a fee) |
| <img src="https://upload.wikimedia.org/wikipedia/commons/8/8b/Copyleft.svg" style="border: none;background: white;box-shadow:none"> | special **license category** allowing free use, modification & distribution |
| | |

<span>**if**<!-- .element: class="fragment" data-fragment-index="1" --></span> <span>these rights are preserved in the adoption<!-- .element: class="fragment" data-fragment-index="1" --></span>

<!--- source:
https://en.wikipedia.org/wiki/Copyleft
-->

---

How does this look in practice?

---

<!-- .slide: data-transition="fade" -->

<img src="https://wiki.creativecommons.org/images/9/97/8256206923_c77e85319e_n.jpg" style="border: none;background: white;box-shadow:none" height="300">

"[Creative Commons 10th Birthday Celebration San Francisco](https://www.flickr.com/photos/sixteenmilesofstring/8256206923/in/set-72157632200936657)"
by tvol
is licensed under [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/)

---

<!-- .slide: data-transition="fade" -->

<img src="https://wiki.creativecommons.org/images/b/b8/8256206923_c77e85319e_n_90fied.jpg
" style="border: none;background: white;box-shadow:none" height="300">

"[90fied](https://wiki.creativecommons.org/images/b/b8/8256206923_c77e85319e_n_90fied.jpg)" by CCID-jane is licensed under [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/) and is a derivative of
"[Creative Commons 10th Birthday Celebration San Francisco](https://www.flickr.com/photos/sixteenmilesofstring/8256206923/in/set-72157632200936657)" by tvol, used under [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/)

---

### Good reference:

1. ✓ name of the work + link to the source(s)
2. ✓ author(s)
3. ✓ license(s) + link to the legal code(s)

<!--- source:
https://wiki.creativecommons.org/wiki/best_practices_for_attribution
-->

---

# CC BY SA

|    |                        |
| -- | ---------------------- |
| CC | Creative Commons       |
| BY | Attribution            |
| SA | ShareAlike (copyleft)  |

---

## 3 flavours of copyleft

| copyleft       | <i class="fa fa-creative-commons"></i> | Software | Hardware |
| -------------- | ---- | ---- | ---- |
| strong         | [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode) | [GPLv3]() | [CERN OHL-S]() |
| weak           | - | [LGPLv3]() | [CERN OHL-W]() |
| non/permissive | [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) | [Apache 2.0]() | [CERN OHL-P]() |

---

<img src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo.svg" style="border: none;background: none;box-shadow:none" height="100"> 

"[Open Source Hardware Logo](https://www.eevblog.com/oshw/)" by [Macklin Chaffee](http://macklinchaffee.com/) is used under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

--

1. sell as proprietary work without giving attribution
2. sell as proprietary work giving attribution
3. modify & redistribute under CC BY 4.0
4. modify & redistribute under CC BY-SA 4.0
5. modify & redistribute under GPLv3

---

1. ~~sell as proprietary work without giving attribution~~
2. ~~sell as proprietary work giving attribution~~
3. ~~modify & redistribute under CC BY 4.0~~
4. modify & redistribute under CC BY-SA 4.0
5. modify & redistribute under GPLv3

<span>--<!-- .element: class="fragment" data-fragment-index="1" --></span>

<span>copyleft = derivatives must remain open!<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

<img src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo.svg" style="border: none;background: none;box-shadow:none" height="100"> 

"[Open Source Hardware Logo](https://www.eevblog.com/oshw/)" by [Macklin Chaffee](http://macklinchaffee.com/) is used under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

1. sell as proprietary work without giving attribution
2. sell as proprietary work giving attribution
3. modify & redistribute under CC BY 4.0
4. modify & redistribute under CC BY-SA 4.0
5. modify & redistribute under GPLv3

---

1. ~~sell as proprietary work without giving attribution~~
2. sell as proprietary work giving attribution
3. modify & redistribute under CC BY 4.0
4. modify & redistribute under CC BY-SA 4.0
5. modify & redistribute under GPLv3

<span>--<!-- .element: class="fragment" data-fragment-index="1" --></span>

<span>permissive = use as you like<!-- .element: class="fragment" data-fragment-index="1" --></span>


---

## More practical examples

Which of the following are legal?

1. <span>✓<!-- .element: class="fragment" data-fragment-index="1" --></span> Merge external software code (GPLv3) into my software code (Apache 2.0)
2. <span>✓<!-- .element: class="fragment" data-fragment-index="1" --></span> Merge external software code (Apache 2.0) into my _proprietary_ software project
3. <span>✓<!-- .element: class="fragment" data-fragment-index="1" --></span> Include proprietary components in the bill of materials of my open source hardware project (CERN OHL-S v2.0)
4. <span>✓<!-- .element: class="fragment" data-fragment-index="1" --></span> Publish technical documentation of my OSH project under GPLv3.

---

## Key takeaways

- <span>once it's copyleft, it's copyleft forever<!-- .element: class="fragment fade-up" data-fragment-index="1" --></span>
- <span>use hardware-/software-specific licenses<!-- .element: class="fragment fade-up" data-fragment-index="2" --></span>
- <span>refer to the source! give attribution<!-- .element: class="fragment fade-up" data-fragment-index="3" --></span>

<span>find a tl;dr guide<!-- .element: class="fragment fade-up" data-fragment-index="4" --></span> <span>[here](https://fairkom.net/nextcloud/index.php/s/HeQ4EMoKBPLriGS)<!-- .element: class="fragment fade-up" data-fragment-index="4" --></span> <span>to look things up.<!-- .element: class="fragment fade-up" data-fragment-index="4" --></span>

---

## Wrap-up

|   | applies for | praxis  |
| -------- | -------- | -------- |
| copyright     | creative act | free/open license |
| patent law | technical invention | defensive publishing |
| trademark | product identity | possible business model |

---

## How to make it open?

---

- What to release?
    - <span>3D models, drawings, PCB layouts<!-- .element: class="fragment" data-fragment-index="1" --></span>
    - <span>descriptions & rationales<!-- .element: class="fragment" data-fragment-index="2" --></span>
    - <span>code<!-- .element: class="fragment" data-fragment-index="3" --></span>
- …in which format?
    - <span>original & export format<!-- .element: class="fragment" data-fragment-index="4" --></span>
- …under which license?
    - <span>CERN OHL-S v2.0 (hardware), GPLv3 (software), CC BY-SA 4.0 (other)<!-- .element: class="fragment" data-fragment-index="5" --></span>
- …and where?
    - <span>80+ OSH platforms (or just GitLab)<!-- .element: class="fragment" data-fragment-index="6" --></span>

Note:
    [Q] That's a repetition, let the course answer these.

---

## A word on communities

> If you want to go faster, go alone.
> If you want to get further, go with others.

---

## It's about sharing

It's all open. If you share your knowledge & skills, others will share their knowledge & skills, too. Don't be shy. Share your problems & needs in the community so we can solve them together.

Note:
    This essentially includes that you _can_ ask other open source businesses about how they did overcome their struggles.

---

# Open revenue streams
---

aka funding

Note:
    There are zillions of new opportunities when you make things open.
    Those are paths that (in hardware) not too many people have tried before you.
    Stability of business models highly depends on your product and ecosystem.
    and is still subject to research and entrepreneurship

---

### Localise the unpaid jobs:

<span>development<!-- .element: class="fragment highlight-red" data-fragment-index="1" --></span>, production, repair, service, individual adoption, <span>documetation<!-- .element: class="fragment highlight-red" data-fragment-index="1" --></span>, recylcing, refurbishment, workshops,…

Note:
    Of course, there's more, but you get the point.

---

There are numerous revenue streams. Entrepreneurs usually

# mix.

<span>Some examples for inspiration:<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

## surcharge 

on products/services

## 
---

`…for products that require special know-how/tools`

(RedHat)

`…for strong trademarks`

(Arduino)

    TENSION
    
    simple product & intuitive documentation
    → users can do it without you

---

## commercial orders

specifically requiring open source

## 
---

`…for niches`
([CERN](https://cds.cern.ch/record/2109248/files/CERN-Brochure-2015-002-Eng.pdf))

    TENSION
    
    …for niches

---

## open core

& proprietary addons

## 
---

`…for use cases that change` 
`when scaled (for business)`

([GitLab Enterprise](https://about.gitlab.com/pricing/))

    TENSION
    
    Community develops your core product, 
    but the cool features are enterprise??

---

## memberships

or long-term donations

## 
---

`…for altruistic projects addressing pressing issues of a large user base`

([FreeCAD](https://www.patreon.com/yorikvanhavre))

    TENSION
    
    the donation market is highly competitive

---

## crowdfunding 
---

`…for viral ideas`

([Flipper Zero](https://www.kickstarter.com/projects/flipper-devices/flipper-zero-tamagochi-for-hackers))
    
    TENSION
    
    ++short term++ funding with a lot PR effort

<!--- source:
https://www.kickstarter.com/projects/flipper-devices/flipper-zero-tamagochi-for-hackers
-->

Note:
    That's a kickstarter project that asked for 60k and got 4m


---

## shared costs

sharing across stakeholders/users of the technology
## 
---

`…for crucial, anchored technologies`

([Open Compute Project](https://www.opencompute.org/membership/membership-organizational-directory))

    TENSION
    
    not an actual revenue stream

---

## voluntary contribution
---

`…for active communities`

([Brewdog](https://www.brewdog.com/community))

    TENSION
    
    not an actual revenue stream
    + the 'market of attention' is highly competitive

---

## public funding

public money, public code

## 
---
`…for research institutes & NPOs`
    
([RepRap](https://researchportal.bath.ac.uk/en/publications/reprap-the-replicating-rapid-prototyper))

    TENSION
    
    usually requires a (hosting) public or non-profit organisation

---

…this could go on for hours

### Questions to ask yourself:

1. Who are your users?
2. Who are your contributors?
3. What problems are you solving?
4. In which scenario would users/funders pay your work?

### <span>Start mixing!<!-- .element: class="fragment" data-fragment-index="1" --></span>
